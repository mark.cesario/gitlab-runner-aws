#!/bin/bash

#Install docker https://phoenixnap.com/kb/how-to-install-docker-on-ubuntu-18-04
sudo apt-get update -y
sudo apt install docker.io -y
usermod -aG docker ubuntu

#Install gitlab runner
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

#Make executable
sudo chmod +x /usr/local/bin/gitlab-runner

#Create gitlab CI user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

#Run the runner as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

#Create config file
#sudo touch /etc/gitlab-runner/config.toml

#Register the runner
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "CfHzye9EVexmFwEz3y2f" \
  --executor "docker" \
  --docker-image hashicorp/terraform \
  --description "docker-runner" \
  --tag-list "curl,run,deploy,stage,qa,build" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

#Start the gitlab-runner
sudo gitlab-runner start
